# %% md
# Benchmarking the Heating case study
# %%

import numpy as np
import polytope as pc
import scipy
import scipy.stats
# list of imports:
import scipy.io as sio
import os
import itertools
import matplotlib.pyplot as plt
from IPython.display import Latex, Math, display

import time

# %%
mat_contents = sio.loadmat(os.getcwd() + '/export2python.mat')
time_start = time.clock()

A = mat_contents['Ahat7']
B = mat_contents['Bhat7']
C = mat_contents['Chat7']
Bw = mat_contents['Fhat7']

# %%
print("1. rescale the Bw matrix")
print("   number of independent standard gaussian disturbances = ", Bw.shape[1])
r = np.linalg.matrix_rank(Bw)
print("   actual rank Bw = ", r)
print("   Compute new Bw, based on SVD")
U, S, V = np.linalg.svd(Bw)
Bw = U[:, :r] * S[:r]

input_space = pc.box2poly(np.array([[-0.5, 0.5]]))  # continuous set of inputs
state_space = (pc.box2poly(np.array([[-.5, .5]]*7)),)
#
print('2. Sanity check: How large is the disturbance in the confidence region 0.99')
# %%
# 6 dimensional standard gaussian distribution
conf = 0.99
conf_i = conf ** (1 / 4)

poly_dist = pc.box2poly(np.array([list(scipy.stats.norm.interval(conf_i, loc=0, scale=1))] * 4))
# six dimensional disturbance polytope
# calculate the interval for standard distribution at 0,
# with variance 1 and confidence conf_i

#
dist = pc.extreme(poly_dist).T
poly_bw = np.diag(S[:r]).dot(dist)
pol = pc.qhull(poly_bw.T)
box = pc.bounding_box(pol)
state_space_box = pc.bounding_box(state_space[0])
print("   bounding box = ", box)
print("   disturbance total size in each direction = ", [box[1][i] - box[0][i] for i in range(4)])

print("   number of cells from which gridding could perform better  = ",
      np.prod(np.array([(state_space_box[1][i] - state_space_box[0][i])
                        / (np.amax(box))
                        for i in range(state_space_box[0].shape[0])])))

print("===> Do not grid, use LTI with truncated noise (hardcoded decision)\n")

# %%

print("3. Compute truncated noise model ")

# 6 dimensional standard gaussian distribution
delta = 0.0001
conf = 1 - delta
conf_i = conf ** (1 / 4)
print("   prob. error at each step: delta= %.8f" % delta)

orig_ = [list(scipy.stats.norm.interval(conf_i, loc=0, scale=1))] * 4
print("   truncated Gaussian noise with hyper-cubic domain", orig_)

# domain of truncated noise
# display(Latex(r'$\tilde M\preceq^{\epsilon = 0}_{\delta = %.2e} M$' % delta))
# display(Latex(r'disturbances $w_1\in [{0:.3f},{1:.3f}] $,'.format(orig_[0][0], orig_[0][1]) +
#               r' $w_2\in [{0:.3f},{1:.3f}] $,'.format(orig_[1][0], orig_[1][1]) +
#               r' $w_3\in [{0:.3f},{1:.3f}] $,'.format(orig_[2][0], orig_[2][1]) +
#               r' $w_4\in [{0:.3f},{1:.3f}] $.'.format(orig_[3][0], orig_[3][1])))
# # %% md
print('4. Forward reachability for disturbance')
# %%

print("   propogate effect of the disturbance by encapulating hyper-cubes")


def forward_box(AB, xbox, wbox):
    dist = np.concatenate([[np.array(list_items).dot(AB.T)]
                           for list_items in itertools.product(*xbox, *wbox)])
    return [row for row in np.array((np.amin(dist, axis=0), np.amax(dist, axis=0))).T]


AB = np.concatenate([A, Bw], axis=1)

x1 = forward_box(AB, ([[0]] * 7), orig_)
x2 = forward_box(AB, x1, orig_)
x3 = forward_box(AB, x2, orig_)
x4 = forward_box(AB, x3, orig_)
x5 = forward_box(AB, x4, orig_)
x6 = forward_box(AB, x5, orig_)



# plt.step(np.arange(0, 7), [x1[0] * 0, x1[0], x2[0], x3[0], x4[0], x5[0], x6[0]])
# plt.xlabel("time")
# plt.ylabel("$y_\Delta$")
# plt.title("Build up of deviation over time for y")
# plt.show()
# %%
print("   Compute effect of disturbance on safe set (for increasing time horizon)")
target = np.array([-.5, .5])
# plt.step(np.arange(0, 7), [target - x1[0] * 0,
#                            target - x1[0],
#                            target - x2[0],
#                            target - x3[0],
#                            target - x4[0],
#                            target - x5[0],
#                            target - x6[0]])
# plt.xlabel("time")
# plt.ylabel("y")
# plt.title("shrinking safe set over time")
# plt.show()

print( [target - x1[0] * 0,
           target - x1[0],
           target - x2[0],
           target - x3[0],
           target - x4[0],
           target - x5[0],
           target - x6[0]])
print('\n----------------------\n')
print("WARNING: No control synthesis needed: when starting at [0,0,0,0,0,0,0] with policy u(t)=0 \n \
       the output will stay in the safe set for the abstract model with prob =1")
print("Similarly, for the original model the satisfaction probability is (1-delta)^6 = %.8f" % ((1 - delta) ** 6))

print("The time needed for this computation is = %.8f" % (time.clock() - time_start))


