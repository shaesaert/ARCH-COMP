function [Asys, Bsys, csys, Ii, Ip, N, f0, fref] = initPLL()
% updated: 10-August-2010, MA

fref=27; %MHz
f0=27e3 - 70; %GHz
%f0=27e3; %GHz
N=1000;

Ki=200; %MHz/V 
Kp=25; %MHz/V

%charge pump
Ii=10e-6; %uA
Ip=500e-6; %uA


%capacitors
Ci=25e-12; %pF
Cp1=6.3e-12; %pF
Cp3=2e-12; %pF

%resistors
Rp2=50e3; %kOhm
Rp3=8e3; %kOhm

%obtain system matrix
%system dimension
dim=4; 

%initialize A
A=zeros(dim);

%set values
A(2,2)=-1e-6/Cp1*(1/Rp2+1/Rp3);
A(2,3)=1e-6/(Cp1*Rp3);
A(3,2)=1e-6/(Cp3*Rp3);
A(3,3)=-1e-6/(Cp3*Rp3);
A(4,1)=Ki/N;
A(4,3)=Kp/N;

%obtain input matrix
%number of inputs
nrOfInputs=2;

%initialize B
B=zeros(dim,nrOfInputs);

%set values
B(1,1)=1e-6/Ci;
B(2,2)=1e-6/Cp1;

%obtain constant vector
%initialize c
%c(4) used to be 1/N*f0 --> not necessary anymore...
%change c value based on change of coordinates later
c = zeros(dim,1);
c(4) = 1/N*f0;

%change c value based on change of coordinates d
%d = [0.5; 0.5; 0.5; 0];
d = [0; 0; 0; 0];
c = A*d + c;

% generate A and B matrices for different saturation regions
% 1: no saturation
% 2: integral saturation
% 3: proportional saturation
% 4: integral and proportional saturation

% define Umin and Umax
Umin = 0.35; 
Umax = 0.5; 

%compute relevant factors
constFactor = Umax/(Umax - Umin);
stateFactor = -1/(Umax - Umin);

%models 2-4 are only valid when the input is switched on!

%no saturation
Asys{1} = A;
Bsys{1} = B;
csys{1} = c;

%integral saturation
Asys{2} = A;
Bsys{2} = B;

Asys{2}(1,1) = Asys{2}(1,1) + stateFactor*Bsys{2}(1,1)*Ii;
Bsys{2}(1,1) = constFactor*Bsys{2}(1,1);
csys{2} = c;

%proportional saturation
Asys{3} = A;
Bsys{3} = B;

Asys{3}(2,2) = Asys{3}(2,2) + stateFactor*Bsys{3}(2,2)*Ip;
Bsys{3}(2,2) = constFactor*Bsys{3}(2,2);
csys{3} = c;


%integral and proportional saturation
Asys{4} = A;
Bsys{4} = B;

Asys{4}(1,1) = Asys{4}(1,1) + stateFactor*Bsys{4}(1,1)*Ii;
Asys{4}(2,2) = Asys{4}(2,2) + stateFactor*Bsys{4}(2,2)*Ip;
Bsys{4}(1,1) = constFactor*Bsys{4}(1,1);
Bsys{4}(2,2) = constFactor*Bsys{4}(2,2);
csys{4} = c;
